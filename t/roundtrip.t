#!/usr/bin/perl

use strict;
use warnings;

use File::Temp;
use Test::More tests => 16;

use EmailReminder::EventList;
use EmailReminder::Event;

# load the data in
my $er_orig = EmailReminder::EventList->new('t/data.xml', 1);

# save it out again
my (undef, $tmp_file) = File::Temp::tempfile();
$er_orig->save(0, $tmp_file);

# load it back and compare all elements
my $er_new = EmailReminder::EventList->new($tmp_file, 1);

# compare top level info
is($er_orig->_get_user_fname, $er_new->_get_user_fname, 'fname');
is($er_orig->_get_user_lname, $er_new->_get_user_lname, 'lname');
is($er_orig->get_user_email, $er_new->get_user_email, 'email');

# compare all the events and make sure they are the same too
my @events_orig = $er_orig->get_events();
my @events_new = $er_new->get_events();

is(scalar @events_orig, scalar @events_new, "same number of events");

for ( my $i = 0; $i < @events_orig; $i++ ) {
    my $orig_event = $events_orig[$i] . "";
    for ( my $j = 0; $j < @events_new; $j++ ) {
        my $new_event = $events_new[$j] . "";
        if ($orig_event eq $new_event) {
            is($orig_event, $new_event, "event $i");
            splice (@events_new, $j, 1);
            last;
        }
    }
}
is(scalar @events_new, 0, "all events were found");
