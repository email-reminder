# -*-Perl-*-

use Test;

BEGIN { plan tests => 14}

eval { require EmailReminder::AnniversaryEvent; return 1;};
ok($@,'');

eval { require EmailReminder::BirthdayEvent; return 1;};
ok($@,'');

eval { require EmailReminder::Event; return 1;};
ok($@,'');

eval { require EmailReminder::EventList; return 1;};
ok($@,'');

eval { require EmailReminder::Utils; return 1;};
ok($@,'');

eval { require EmailReminder::MonthlyEvent; return 1;};
ok($@,'');

eval { require EmailReminder::WeeklyEvent; return 1;};
ok($@,'');

eval { require EmailReminder::YearlyEvent; return 1;};
ok($@,'');

eval { require EmailReminder::AnniversaryStore; return 1;};
ok($@,'');

eval { require EmailReminder::BirthdayStore; return 1;};
ok($@,'');

eval { require EmailReminder::EventStore; return 1;};
ok($@,'');

eval { require EmailReminder::MonthlyStore; return 1;};
ok($@,'');

eval { require EmailReminder::WeeklyStore; return 1;};
ok($@,'');

eval { require EmailReminder::YearlyStore; return 1;};
ok($@,'');
