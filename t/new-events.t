#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 10;

use Date::Manip;
use EmailReminder::EventList;
use EmailReminder::Event;

# load the data in
my $er = EmailReminder::EventList->new('t/empty.xml', 1);

my $event;

$event = $er->create_event('monthly');
$event->set_name('New Monthly');
is("$event", "monthly:0) New Monthly - 1", 'new monthy');
is($event->get_nb_fields(), 3, 'monthly fields');

$event = $er->create_event('weekly');
$event->set_name('New Weekly');
is("$event", "weekly:1) New Weekly - 7", 'new weekly');
is($event->get_nb_fields(), 3, 'weekly fields');

$event = $er->create_event('birthday');
$event->set_name('New Birthday');
is("$event", "birthday:2) New Birthday - 01-01", 'new birthday');
is($event->get_nb_fields(), 4, 'birthday fields');

$event = $er->create_event('anniversary');
$event->set_name('New Anniversary');
is("$event", "anniversary:3) New Anniversary and  - 01-01", 'new anniversary');
is($event->get_nb_fields(), 6, 'anniversary fields');

$event = $er->create_event('yearly');
$event->set_name('New Yearly');
is("$event", "yearly:4) New Yearly - 01-01", 'new yearly');
is($event->get_nb_fields(), 3, 'yearly fields');
