#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 5;

use EmailReminder::EventList;
use EmailReminder::Event;

# load the data in
my $er = EmailReminder::EventList->new('t/data.xml', 1);

# create a few events but change some details
my $event;

# add a yearly event first
$event = $er->create_event( EmailReminder::YearlyEvent->get_type() );
isa_ok($event, 'EmailReminder::YearlyEvent', 'yearly event');

# monthly
$event = $er->create_event( EmailReminder::MonthlyEvent->get_type() );
isa_ok($event, 'EmailReminder::MonthlyEvent', 'monthly event');

# weekly
$event = $er->create_event( EmailReminder::WeeklyEvent->get_type() );
isa_ok($event, 'EmailReminder::WeeklyEvent', 'weekly event');

# birthday
$event = $er->create_event( EmailReminder::BirthdayEvent->get_type() );
isa_ok($event, 'EmailReminder::BirthdayEvent', 'birthday event');

# anniversary
$event = $er->create_event( EmailReminder::AnniversaryEvent->get_type() );
isa_ok($event, 'EmailReminder::AnniversaryEvent', 'anniversary event');
