#!/usr/bin/perl

=head1 NAME

Email-Reminder-Editor - edit special occasion reminders

=head1 SYNOPSIS

Simple editor for modifying special occasion email reminders.

=head1 DESCRIPTION

Email-reminder allows users to define events that they want to be
reminded of by email.  Possible events include birthdays,
anniversaries and yearly events.  Reminders can be sent on the day of
the event and a few days beforehand.

This is a simple editor that allows users to add/modify their
reminders.  It saves changes automatically when the program is closed.

=head1 OPTIONS

=over 6

=item B<--help>

Displays basic usage message.

=item B<--simulate>

Does not actually save any changes.

=item B<--verbose>

Prints out information about what the program is doing.

=item B<--version>

Displays the version number.

=back

=head1 FILES

F<~/.email-reminders>

=head1 AUTHOR

Francois Marier <francois@fmarier.org>

=head1 SEE ALSO

collect-reminders, send-reminders

=head1 COPYRIGHT

Copyright (C) 2004-2014 by Francois Marier

Email-Reminder is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

Email-Reminder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with Email-Reminder; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

=cut

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;

use Gtk2 '-init';
use Gtk2::SimpleList;
use Gtk2::SimpleMenu;

use constant TRUE => 1;
use constant FALSE => 0; 

use EmailReminder::Event;
use EmailReminder::EventList;

# Command-line parameters
my $verbose = 0;
my $debug = 0;
my $simulate = 0;
my $version = 0;
my $help = 0;
GetOptions("verbose"  => \$verbose, 
           "debug"    => \$debug, 
           "simulate" => \$simulate,
           "version"  => \$version,
	   "help"     => \$help,
           );

# Constants
my $ANNIVERSARY_TAB = 'Anniversaries';
my $BIRTHDAY_TAB = 'Birthdays';
my $MONTHLY_TAB = 'Monthly Events';
my $WEEKLY_TAB = 'Weekly Events';
my $YEARLY_TAB = 'Yearly Events';

my $EMAIL_PREF_LABEL = 'Email:';
my $NAME_PREF_LABEL = 'Name:';

my $NEW_EVENT_NAME = '<new event>';
my $WINDOW_TITLE = 'Email-Reminder Editor';

my $DEFAULT_WIDTH = 600;
my $DEFAULT_HEIGHT = 400;

my $SEND_REMINDERS_EXECUTABLE = '/usr/bin/send-reminders';

# Global variables
my $window;
my $notebook;
my $events;

sub load_data
{
    $events = EmailReminder::EventList->new(glob("~/" . $EmailReminder::Utils::USER_CONFIG_FILE), TRUE);
    print "Events loaded.\n" if $verbose;
    return;
}

sub save_data
{
    $events->save($verbose) unless $simulate;
    print "Events saved.\n" if $verbose;
    return;
}

sub text_cell_edited
{
    my ($cell_renderer, $text_path, $new_text, $model) = @_;
    my $path = Gtk2::TreePath->new_from_string($text_path);
    $model->set_value($path, $cell_renderer->{column}, $new_text);
    return 1;
}

sub sort_by_column
{
    return 0;
}

sub create_listbox
{
    my $type = shift;
    my @columns = ('ID', @_);

    # TODO: make listbox sortable by clicking on column headers
    my $model = $events->get_model($type);
    #my $sort_model = Gtk2::TreeModelSort->new_with_model($model);
    #$sort_model->set_default_sort_func(\&_sort_by_column);
    #$sort_model->set_sort_column_id(1, 'ascending');
    my $list = Gtk2::TreeView->new($model);
    $list->{type} = $type;

    my $col_num = 0;
    foreach my $title (@columns) {
	my $renderer = Gtk2::CellRendererText->new();
	$renderer->set(editable => TRUE);
	$renderer->{column} = $col_num;
	$renderer->signal_connect (edited => \&text_cell_edited, $model);

	my $col = Gtk2::TreeViewColumn->new_with_attributes($title, $renderer, 'text' => $col_num);
	$col->set_resizable(TRUE);
	#$col->set_clickable(FALSE);
	#$col->set_sort_column_id($col_num);
	$list->append_column($col);

	$col_num++;
    }

    $list->get_column(0)->set_visible(FALSE) unless $debug; # hide the ID column
    
    my $scrolled = Gtk2::ScrolledWindow->new;
    $scrolled->set_policy('automatic', 'automatic');
    $scrolled->add($list);
    
    return $scrolled;
}

sub create_menu
{
    my $menu_tree = [
                     _File => {
                         item_type => '<Branch>',
                         children => [
                                      '_Test configuration' => {
                                          callback => \&test_callback,
                                          callback_action => 0,
                                      },
                                      Separator => {
                                          item_type => '<Separator>',
                                      },
                                      _Quit => {
                                          item_type => '<StockItem>',
                                          callback => \&window_close,
                                          extra_data => 'gtk-quit',
                                      },
                                      ],
                     },
                     _Edit => {
                         item_type => '<Branch>',                     
                         children => [
                                      '_New event' => {
                                          item_type => '<StockItem>',
                                          extra_data => 'gtk-new',
                                          callback => \&new_callback,
                                      },
                                      '_Delete event' => {
                                          item_type => '<StockItem>',
                                          extra_data => 'gtk-delete',
                                          accelerator => '<ctrl>D',
                                          callback => \&delete_callback,
                                      },
                                      '_Edit reminders' => {
                                          callback => \&edit_callback,
                                      },
                                      Separator => {
                                          item_type => '<Separator>',
                                      },
                                      '_Preferences...' => {
                                          item_type => '<StockItem>',
                                          extra_data => 'gtk-preferences',
                                          callback => \&prefs_callback,
                                      },
                                      ]
                                      }
                     ];

    return Gtk2::SimpleMenu->new(menu_tree => $menu_tree);
}

sub test_callback
{
    unless (-x $SEND_REMINDERS_EXECUTABLE) {
        my $error = Gtk2::MessageDialog->new($window, ['modal'], 'error', 'ok', "Cannot run '$SEND_REMINDERS_EXECUTABLE'.  Check your installation.");
        $error->run();
        $error->destroy();
        return;
    }

    save_data();
    my $errorOutput = `$SEND_REMINDERS_EXECUTABLE`;
    if ($errorOutput) {
        my $error = Gtk2::MessageDialog->new($window, ['modal'], 'error', 'ok', $errorOutput);
        $error->run();
        $error->destroy();
	return;
    }
    return 1;
}

sub new_callback
{
    my (undef, $listbox) = get_selected_index();

    my $event_type = $listbox->{type};
    $events->add_event($event_type);

    my $event_index = $listbox->get_model()->get_nb_events() - 1;
    $listbox->get_selection()->select_path(Gtk2::TreePath->new_from_string($event_index));
    return 1;
}

sub delete_callback
{
    my ($path, $listbox) = get_selected_index();
    return unless defined($path);
    
    $listbox->get_model()->delete_event($path);
    return 1;
}

sub edit_callback
{
    my $selected = get_selected_event();
    return unless defined($selected);
    
    my $dialog = create_reminder_dialog($selected);
    $dialog->show_all;
    return 1;
}

sub prefs_callback
{
    my $dialog = create_prefs_dialog();
    $dialog->run();
    $dialog->destroy();
    return 1;
}

sub create_reminder_dialog
{
    my ($event) = @_;

    my $name = $event->get_name();
    my $reminders = $event->get_reminders();

    my $dialog = Gtk2::Dialog->new_with_buttons('Edit reminders', $window, 
                                                'destroy-with-parent',  
                                                'gtk-close' => 'close' );

    my $reminder_label = Gtk2::Label->new("Current reminders for '$name':");
    my $cb_sameday = Gtk2::CheckButton->new("Same day");
    my $cb_advance = Gtk2::CheckButton->new("Days in advance:");
    my $adj = Gtk2::Adjustment->new(1.0, 1.0, 364, 1.0, 10.0, 0.0);
    my $spin_days = Gtk2::SpinButton->new($adj, 0, 0); 

    # Disable spin button unless the option is checked
    $spin_days->set_sensitive(FALSE);
    $cb_advance->signal_connect(toggled => sub {
        $spin_days->set_sensitive($cb_advance->get_active());
    });

    my $hbox = Gtk2::HBox->new();
    $hbox->add($cb_advance);
    $hbox->add($spin_days);

    my $checkboxes = Gtk2::VBox->new(FALSE, 6);
    $checkboxes->set_border_width(6);
    $checkboxes->add($reminder_label);
    $checkboxes->add($cb_sameday);
    $checkboxes->add($hbox);

    foreach my $reminder (@$reminders)
    {
        if ($reminder == 0)
        {
            $cb_sameday->set_active(TRUE);
        }            
        elsif ($reminder > 0)
        {
            $cb_advance->set_active(TRUE);
            $spin_days->set_value($reminder);
        }
    }
        
    $checkboxes->show_all;
    $dialog->vbox->add($checkboxes);

    $dialog->signal_connect(response => sub {
        # Update values inside EventList
        my @new_reminders = ();
        push(@new_reminders, 0) if $cb_sameday->get_active();
        push(@new_reminders, $spin_days->get_value())
            if $cb_advance->get_active();
        $event->set_reminders(\@new_reminders);

        $_[0]->destroy;
    });  
    
    return $dialog;
}

sub create_prefs_dialog
{
    my $dialog = Gtk2::Dialog->new_with_buttons('Preferences', $window, 
                                                'modal',  
                                                'gtk-close' => 'close' );

    my $info_label = Gtk2::Label->new("Set the default recipient for the reminder emails:");
    my $name_label = Gtk2::Label->new($NAME_PREF_LABEL);
    my $email_label = Gtk2::Label->new($EMAIL_PREF_LABEL);

    my $fname = Gtk2::Entry->new();
    my $lname = Gtk2::Entry->new();
    my $email = Gtk2::Entry->new();
    my @fullname = $events->get_user_name();
    $fname->set_text($fullname[0]);
    $lname->set_text($fullname[1]);
    $email->set_text($events->get_user_email());

    my $name = Gtk2::HBox->new();
    $name->pack_start($fname, TRUE, TRUE, 0);
    $name->pack_start($lname, TRUE, TRUE, 0);

    my $options = Gtk2::Table->new(3, 2, FALSE);
    $options->set_row_spacings(3);
    $options->attach_defaults($name_label, 0, 1, 0, 1);
    $options->attach_defaults($name, 1, 2, 0, 1);
    $options->attach_defaults($email_label, 0, 1, 1, 2);
    $options->attach_defaults($email, 1, 2, 1, 2);

    $options->show_all();
    $info_label->show();
    $dialog->vbox->set_spacing(6);
    $dialog->vbox->add($info_label);
    $dialog->vbox->add($options);

    $dialog->signal_connect(response => sub {
        # Update values
        $events->set_user_fname($fname->get_text());
        $events->set_user_lname($lname->get_text());
        unless ($events->set_user_email($email->get_text())) {
            my $warning = Gtk2::MessageDialog->new($dialog, ['modal'], 'warning', 'ok', "The email address you entered is invalid; it has not been changed.");
            $warning->run();
            $warning->destroy();
        }
    });
    
    return $dialog;
}

sub window_close
{
    unless ($events->get_user_email())
    {
        my $warning = Gtk2::MessageDialog->new($window, ['modal'], 'warning', 'none', "You will not receive any reminders since you have not set your email address. \n\nWould you like to set your email address in the preferences now or quit?");
        $warning->add_buttons('gtk-preferences' => 'no', 'gtk-quit' => 'yes');

        my $response = $warning->run();
        $warning->destroy();

        if ('no' eq $response)
        {
            prefs_callback();
            return TRUE;
        }
    }
    
    $window->destroy();
    return 1;
}

sub init_ui
{
    $window = Gtk2::Window->new;
    $window->set_title($WINDOW_TITLE);
    $window->set_default_size($DEFAULT_WIDTH, $DEFAULT_HEIGHT);
    $window->set_resizable(TRUE);

    $window->signal_connect(destroy => sub { Gtk2->main_quit; });
    $window->signal_connect(delete_event => \&window_close);

    my $vbox = Gtk2::VBox->new(FALSE, 0);
    $window->add($vbox);

    # Menu
    my $menu = create_menu();
    $window->add_accel_group($menu->{accel_group});
    $vbox->pack_start($menu->{widget}, FALSE, FALSE, 0);

    # Toolbar
    my $toolbar = Gtk2::Toolbar->new() ;
    $toolbar->set_style('both-horiz');
    $toolbar->insert_stock('gtk-new', "Add a new event", undef, \&new_callback, undef, -1);
    $toolbar->insert_stock('gtk-delete', "Delete the selected event", undef, \&delete_callback, undef, -1);
    $toolbar->append_space();
    $toolbar->insert_item("Edit reminders", "Edit reminders for the selected event", undef, undef, \&edit_callback, undef, -1);
    $vbox->pack_start($toolbar, FALSE, FALSE, 0);

    # Tabs
    # TODO: make the accel Ctrl+PageDown/Up work everywhere 
    #       (not just when focus is on the tabs)
    $notebook = Gtk2::Notebook->new();
    $notebook->set_tab_pos('top');
    $vbox->pack_start($notebook, TRUE, TRUE, 0);

    # Lists
    my $listbox1 = create_listbox("birthday", 'Name', 'Birth date', 'Email');
    my $listbox2 = create_listbox("anniversary", 'Person 1', 'Wedding date', 'Email 1', 'Person 2', 'Email 2');
    my $listbox3 = create_listbox("yearly", 'Event name', 'Event date');
    my $listbox4 = create_listbox("monthly", 'Event name', 'Event day');
    my $listbox5 = create_listbox("weekly", 'Event name', 'Event day');
    $notebook->append_page($listbox1, $BIRTHDAY_TAB);
    $notebook->append_page($listbox2, $ANNIVERSARY_TAB);
    $notebook->append_page($listbox3, $YEARLY_TAB);
    $notebook->append_page($listbox4, $MONTHLY_TAB);
    $notebook->append_page($listbox5, $WEEKLY_TAB);

    return 1;
}

sub get_selected_index
{
    my $scrolled = $notebook->get_nth_page($notebook->get_current_page());
    my $listbox = $scrolled->get_child();

    my $path = $listbox->get_selection()->get_selected_rows();
    return ($path, $listbox);
}

sub get_selected_event
{
    my ($path, $listbox) = get_selected_index();
    return unless defined($path);

    my $event = $listbox->get_model()->get_event($path);
    return $event;
}

sub run_gui
{
    $window->show_all();
    Gtk2->main;
    return 1;
}

sub main
{
    print "Version: $EmailReminder::Utils::VERSION\n" if $verbose;
    load_data();
    init_ui();
    run_gui();
    save_data();
    return 1;
}

if ($help || $version) {
    print "$WINDOW_TITLE $EmailReminder::Utils::VERSION\n";
    if ($help) {
	print "\n";
	pod2usage(1);
    } else {
	exit(1);
    }
} else {
    main();
}
