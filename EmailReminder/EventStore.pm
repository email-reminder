# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::EventStore;

# Base class for all of the event stores.
# 
# This class should never be used directly, use a derived class instead.

use strict;
use warnings;

use Gtk2;
use Glib::Object::Subclass
    Glib::Object::,
    interfaces => [ Gtk2::TreeModel:: ],
    ;

# Column indices
my $ID_INDEX = 0;

sub init
{
    my ($self) = @_;
    $self->{EVENTS} = [];
    $self->{NB_EVENTS} = 0;
    return 1;
}

sub add_event
{
    my ($self, $event) = @_;
    push (@{$self->{EVENTS}}, $event);
    my $path = Gtk2::TreePath->new_from_string($#{$self->{EVENTS}});
    my $iter = $self->get_iter($path);
    $self->row_inserted($path, $iter);
    $self->{NB_EVENTS}++;
    return $path;
}

sub delete_event
{
    my ($self, $path) = @_;
    my $index = $path->get_indices();

    $self->{EVENTS}->[$index]->unlink_event();
    splice(@{$self->{EVENTS}}, $index, 1);
    $self->{NB_EVENTS}--;

    # Send the necessary signals
    $self->row_deleted($path);
    if ($self->{NB_EVENTS} > 0) {
	my $iter = $self->get_iter($path);
        return unless defined($iter);

	$iter = $self->iter_next($iter);
	while (defined($iter)) {
	    my $path = $self->get_path($iter);
	    $self->row_changed($path, $iter);
	    $iter = $self->iter_next($iter);
	}
    }
    return 1;
}

sub get_nb_events
{
    my ($self) = @_;
    return $self->{NB_EVENTS};
}

sub get_event
{
    my ($self, $path) = @_;
    my $index = $path->get_indices();
    return $self->{EVENTS}->[$index];
}

sub get_events
{
    my ($self) = @_;
    return $self->{EVENTS};
}

sub get_event_column
{
    my ($self, $event, $col) = @_;

    if ($col == $ID_INDEX) {
        return $event->get_id();
    }
    else {
        warn "Column '$col' is not a valid column.\n";
        return;
    }
}

sub set_event_column
{
    my ($self, $event, $col, $new_value) = @_;
    if ($col == $ID_INDEX) {
        warn "The ID column is read-only.\n";
    }
    else {
        warn "Column '$col' is not a valid column for value '$new_value'.\n";
    }
    return 1;
}

sub set_value
{
    my ($self, $path, $column, $new_value) = @_;
    my $row_index = $path->get_indices();
    my $event = $self->{EVENTS}->[$row_index];
    $self->set_event_column($event, $column, $new_value);
    return 1;
}

########################
# Tree Model Interface #
########################

sub GET_FLAGS
{
    return 'list-only';
}

sub GET_N_COLUMNS
{
    my $self = shift;
    return $self->{NB_COLUMNS};
}

sub GET_COLUMN_TYPE
{
    return 'Glib::String';
}

sub GET_ITER
{
    my ($self, $path) = @_;

    my $index = $path->get_indices();
    my $iter = [ $index, $index, undef, undef ];

    # Find the first non-deleted item
    unless (exists($self->{EVENTS}->[$index])) {
	$iter = $self->ITER_NEXT($iter);
    }

    return $iter;
}

sub GET_PATH
{
    my ($self, $iter) = @_;
    return Gtk2::TreePath->new($iter->[1]);
}

sub GET_VALUE
{
    my ($self, $iter, $column) = @_;
    my $index = $iter->[1];
    my $event = $self->{EVENTS}->[$index];
    return "(missing)" unless defined($event); # TODO: remove this
    my $value = $self->get_event_column($event, $column);
    return defined($value) ? $value : "";
}

sub ITER_NEXT
{
    my ($self, $iter) = @_;
    my $index = $iter->[1] + 1;

    # Skip over deleted items
    while ($index < @{$self->{EVENTS}}) {
	if (exists($self->{EVENTS}->[$index])) {
	    return [ $index, $index, undef, undef ];
	}
	$index++;
    }

    return;
}

# This is a list store, there are no children

sub ITER_CHILDREN
{
    return;
}

sub ITER_HAS_CHILD
{
    return 0;
}

sub ITER_N_CHILDREN
{
    return 0;
}

sub ITER_NTH_CHILD
{
    return;
}

sub ITER_PARENT
{
    return;
}

1;
